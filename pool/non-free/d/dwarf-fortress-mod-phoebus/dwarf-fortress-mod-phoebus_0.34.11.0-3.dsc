-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: dwarf-fortress-mod-phoebus
Binary: dwarf-fortress-mod-phoebus
Architecture: all
Version: 0.34.11.0-3
Maintainer: Beren Minor <beren.minor+debian@gmail.com>
Homepage: http://www.bay12games.com/dwarves/
Standards-Version: 3.9.3
Vcs-Browser: https://github.com/berenm/dwarf-fortress/
Vcs-Git: git://github.com/berenm/dwarf-fortress.git
Build-Depends: debhelper (>= 9), python
Package-List: 
 dwarf-fortress-mod-phoebus deb non-free/games extra
Checksums-Sha1: 
 1faa0ffc6ca1fe5e518ab6590193386e73d2554b 734997 dwarf-fortress-mod-phoebus_0.34.11.0.orig.tar.gz
 eb0737a1420021b7f16c224b41314cf7a5d17dd4 2276 dwarf-fortress-mod-phoebus_0.34.11.0-3.debian.tar.gz
Checksums-Sha256: 
 6249a6a8bcba9309000805d44a6ce14182c928a641e1d08acc2db573ef352729 734997 dwarf-fortress-mod-phoebus_0.34.11.0.orig.tar.gz
 2a00ffdb650c5cdaeca906fe2c36179dfae3b11d96e6940a7ef024d12b62d512 2276 dwarf-fortress-mod-phoebus_0.34.11.0-3.debian.tar.gz
Files: 
 91f3b9886a4ae080c81e6d99f1535e24 734997 dwarf-fortress-mod-phoebus_0.34.11.0.orig.tar.gz
 dc1f1d3fc41a1c8dca91556eef34e61a 2276 dwarf-fortress-mod-phoebus_0.34.11.0-3.debian.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.12 (GNU/Linux)

iQIcBAEBAgAGBQJP44UhAAoJEIX7t2k16B/F4DsP/050GyG0Gf8tJEDUoEVSmY24
wkOsZ2M53T5bf9SYVAHM4H3oDPORJ4Dm2XdObVj+g0XXU88o7ixLjZlItdFIFpcb
MR135PcJ5/Mc4EdF85I/bK+VoK28kJTh38/KruqFjthUDD3mg8+lS1WmPE6JSdUz
fsyeAI0EKthoq66b2eR1ud3HyKYkwbDUPZuHJsN3xttZFBLR2icUlQv81ykl40hE
hv1sJ/slYda3WxvM4frS0/MI3JuFCw15ecTXp+9gSj8J6eFfQrtnYyQ2gzb4Uws+
e38Bd0uYvhuXahU7K4EtsIrbUUh6qCefyHAwHBZfMqwRJ42FM0WRScMfSST5we79
EV/ymJ/3vMuMjRUY0nEEwkv94YbEHPy6ymS6NzB6ER4maX7hrtdHZThCTJUiP5ir
mvCo1tsBgcMM1XUXbUoO7W9IYhiHOU+HZGucV/vpgt3CDyd3cljOsHn/f3nUpEMx
maEgBKuPFwGNzQ6dbqrT7itZmTKThTQi9Xv2yvWMueoLtVLkGQM07OI/rd3+t2nY
yPa7wgpMVa4dNvV2bxXeiYe5FT5pYbtRFP3P8gGue1dXi2TES54PrLMJSdDYf98C
28lPwHtnH9cFbwq+vR/BOzmG/lvXo4J9hD75ELDlotxSBVdJ6/0jzKz+jOR6c9KD
EdVIYgOZ0JbWgseM9Pcx
=JiOu
-----END PGP SIGNATURE-----
