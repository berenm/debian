-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: dwarf-fortress-mod-default
Binary: dwarf-fortress-mod-default
Architecture: all
Version: 0.34.11.0-3
Maintainer: Beren Minor <beren.minor+debian@gmail.com>
Homepage: http://www.bay12games.com/dwarves/
Standards-Version: 3.9.3
Vcs-Browser: https://github.com/berenm/dwarf-fortress/
Vcs-Git: git://github.com/berenm/dwarf-fortress.git
Build-Depends: debhelper (>= 9), python
Package-List: 
 dwarf-fortress-mod-default deb non-free/games extra
Checksums-Sha1: 
 55f4b4b5d210aa2916e8ac4b1aadc198c71354aa 314298 dwarf-fortress-mod-default_0.34.11.0.orig.tar.gz
 00386af552f8d2c7661dd40d57b424662d2d2cde 2262 dwarf-fortress-mod-default_0.34.11.0-3.debian.tar.gz
Checksums-Sha256: 
 8403d3138ada918a639073cb7f9e8c9a94a92dc688acb3e7182d78962a5f1e07 314298 dwarf-fortress-mod-default_0.34.11.0.orig.tar.gz
 0b6b704b60ec7c1ffa3671b34d7e68dd624aa5e43859c9347b193202f7101d65 2262 dwarf-fortress-mod-default_0.34.11.0-3.debian.tar.gz
Files: 
 113144325c9c03f419ad2891dbfd4245 314298 dwarf-fortress-mod-default_0.34.11.0.orig.tar.gz
 eb4e947ddf7c3fa69f683fb432b4cd8d 2262 dwarf-fortress-mod-default_0.34.11.0-3.debian.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.12 (GNU/Linux)

iQIcBAEBAgAGBQJP44ToAAoJEIX7t2k16B/FtY8P/24szUA2k+f4/ueJCKYmGCap
bmdIL2rIc3vjC9rAfU6czZZaT7WiQDmig8nRtPazfkC8GUHg1AhmbHBbxBmZVgS1
5QWLSRG6k0TffcRKASKh8nTJDVbvh9ErLZagGVGYMWSjOphYdbIJ9cpEjSbhLNIQ
xUS1yaAQecPtN39t6xJtDvbAzB+fAS6y4uGhP/49W1hVdIkRRDBJwlIb8h7j6qp6
psZViJOCocfs0nkdG89d8uh97DAgIxaus6eHwMdNZPV+4L1q+lnQIAnG8S5jyi6F
WJDYXseo0peQ489/LB5K0f0Vb8+nO9OLIOGpsZnjTFFkUuOJvPMcWjEQ+aqPHtmx
AF60/75y9aq3S+XgFqKtFdBwFHOYiPFfVMisT+VVvwmLNv2SuoXlJoFZ9RWDSQY0
0VUFRYX7SQeWDAtVKbnMvEE8lQur+5Kk9t4jzfWF/cAiA/1BYMgzTyqmnf4mol17
A5EqswZU0T/LznTE4G7RGrM3AKSle3gw0GAjma43wAZkdex/vxr54OjbEYkXGLDn
Pb2Ewrdv0tZDKWhviuVHSvgVdJNvxyInRMezRVf8+nM16ed9IdCte8O7nLBja58U
L8fgJgLPk9nuI3JgpF3TPISqaofLoSK0gKpAlq5zxouNnhx7esv7yvh+OZvPNid0
5on3+GSI2TrmB3+Bnl/+
=37fy
-----END PGP SIGNATURE-----
