-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: dwarf-fortress-mod-ironhand
Binary: dwarf-fortress-mod-ironhand
Architecture: all
Version: 0.34.11.0-3
Maintainer: Beren Minor <beren.minor+debian@gmail.com>
Homepage: http://www.bay12games.com/dwarves/
Standards-Version: 3.9.3
Vcs-Browser: https://github.com/berenm/dwarf-fortress/
Vcs-Git: git://github.com/berenm/dwarf-fortress.git
Build-Depends: debhelper (>= 9), python
Package-List: 
 dwarf-fortress-mod-ironhand deb non-free/games extra
Checksums-Sha1: 
 e34bec572394a6528d1aa527bdc8af8736508589 997978 dwarf-fortress-mod-ironhand_0.34.11.0.orig.tar.gz
 20f713e79074e590a0080c6b1fb1007b967027c0 2263 dwarf-fortress-mod-ironhand_0.34.11.0-3.debian.tar.gz
Checksums-Sha256: 
 db9848fb2522048b46c9bc5a9feb641fa0187c3854f8c33f220f070edbd9344d 997978 dwarf-fortress-mod-ironhand_0.34.11.0.orig.tar.gz
 a9f1c6f371c5a6c087de608dc6d70183ae49100a1c742a122ae3eaa31bce58eb 2263 dwarf-fortress-mod-ironhand_0.34.11.0-3.debian.tar.gz
Files: 
 b6e0e5d51d5e7d80e2133d7a60ba5393 997978 dwarf-fortress-mod-ironhand_0.34.11.0.orig.tar.gz
 cf322895965c87914e4e1e3933fe2610 2263 dwarf-fortress-mod-ironhand_0.34.11.0-3.debian.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.12 (GNU/Linux)

iQIcBAEBAgAGBQJP44UOAAoJEIX7t2k16B/Fq1wP/iybvXF40gnywJazzAw7FDIX
j5n0z7X6ueN49idnjnJTEBHk35yvHbLh9Xcyr6GbKeaohO8uQR10AiQz8AMN1buD
zEsVFo7z2kb6RG7etCuaNmWVmRgrNMGC1kusSmzjqJv9CzXy/N5JQdNJIkWeAuq4
VAA9wHbCFNdrABltY6v/x3lwdaoKkZ4f22f+kkYy8Bp8TazPl+ZE15r55d2NTKzy
zIoS0oWzZQ3CDpz+aUUAcU0U4vueW2Euroejs8vMn6xrTpYI1OmYBozEJSS1igXv
Zaz0BcTgHVse+TFX6vat2aQ7LrX3DCGOdg0IylFsWn9bx/uUbAiIJp0demi9snSy
oWHIaIQYPLJq8xLynt0jlVX46hgI1wl26hDFdvhuZAXVdyrAA0tz0QoWbH85ROur
L/vqFKMhtrmZGeiYMnWdiEW6nWqTbQn7T2ikrTOc5oBwSl9SknO0/6ufVmCWBm3D
3LXTh1Lm+z8wlyWl6W7VCTmceBjXUavYBNpXJlOEFYwqbTyJh/5RYfhKMtzw96TH
o6lTnTEXjfpXKtbsTk054ObwZOGHoePtqIfKRqTDJa0zILQtVoTm82bZBg019C+D
8eS3icttWQouVd0tgVPV9Ut0eEFrT8sGjDSeTnqBN9e1kzlsRjax1COglvPErFnD
84/0lXcAC3yAbqU7NT6U
=wyXj
-----END PGP SIGNATURE-----
