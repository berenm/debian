-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: dwarf-fortress
Binary: dwarf-fortress-data, libdwarf-fortress-graphics0, dwarf-fortress-bin, dwarf-fortress
Architecture: any all
Version: 0.34.11.0-5
Maintainer: Beren Minor <beren.minor+debian@gmail.com>
Homepage: http://www.bay12games.com/dwarves/
Standards-Version: 3.9.3
Vcs-Browser: https://github.com/berenm/dwarf-fortress/
Vcs-Git: git://github.com/berenm/dwarf-fortress.git
Build-Depends: debhelper (>= 9), python, docbook-xsl, docbook-xml, xsltproc, cmake, libsdl1.2-dev, libsdl-image1.2-dev, libsdl-ttf2.0-dev, libopenal-dev, libsndfile1-dev, libgtk2.0-dev, libncursesw5-dev, libncurses-dev, libz-dev, libglew-dev, libgl1-mesa-dev, libglu-dev
Package-List: 
 dwarf-fortress deb non-free/games extra
 dwarf-fortress-bin deb non-free/games extra
 dwarf-fortress-data deb non-free/games extra
 libdwarf-fortress-graphics0 deb non-free/games extra
Checksums-Sha1: 
 199eb7a6a42daf1946bb749678ed74983d99065a 11851583 dwarf-fortress_0.34.11.0.orig.tar.gz
 e77a4c733e09b87a94545453fa03a856f12f65c8 21946 dwarf-fortress_0.34.11.0-5.debian.tar.gz
Checksums-Sha256: 
 8c3f6e51cc8a097a0f9d88de42ebfd767a28d2ace47b9709ec3e5d2ab8c83117 11851583 dwarf-fortress_0.34.11.0.orig.tar.gz
 3f4a511da986b648050f6432ecfb1a72c531581b974b0da4641f91ee751ddc67 21946 dwarf-fortress_0.34.11.0-5.debian.tar.gz
Files: 
 50b47b276c2253f28a8b20aae9765fc1 11851583 dwarf-fortress_0.34.11.0.orig.tar.gz
 d1caf9ed7f2bada6e0d898f92feb9046 21946 dwarf-fortress_0.34.11.0-5.debian.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.12 (GNU/Linux)

iQIcBAEBAgAGBQJP44OHAAoJEIX7t2k16B/F20gP/26WQVA1bckHIQaO+kqbsuqI
TXqPmZJN3Z1qE/puXGaeDeoapB8x+TQlwth+Co0skDEWHO9PZ9E2/ZkJZhNwMmHu
ct+fr+BhueNOitcJj1TQClb4V1DCth7XYgLmmaJz7W+r9PFuFACCW+QQa+k8Uj+G
OL758b5hLWV18RsW+Td5Za5kmjPNkZ61q4EHuwo2qeg6njLkNzjFCudNl4nxM23q
/9KL+1r4i8Akr44eJ6QFWS/CPmMQ8py5TXtAcpKzpaFtmXS524QJel05lCzYcvsy
BDMMIItyrR54J/RdiuXxbIxwcWh6ecIXzlc2hx1z+irYk94iht2tVeQy76ovHKyW
WlZFkViQogBQncd6jMKEwiEmmEb75tagHk2e5+SQvCpf93qSlzstwhVALRhdhkdo
4ydo48WRb/ZcRTVms8GLNqUYPfaBx/wfOpDC6eLpPgAVx0DT/xRfL/VIg+c/PMFz
pMg73nXuCrwQQENuVkYrlL7MycXOINu46H4zOmO9VcAH67viXOiMPrYXFVgjWN9V
TGx9sfmoOns31ncUkaEuf/el2xNmMbobHkT/IRSReCHIt0x6GB81zPWwnekGFMZE
nJiWVOspBQxHnbt4XABtbeMqoQIutLtpOoKhH6I6RVapZI6hYIWQmgx3khoC+Jle
Khz+le8xNZjQ0GPnGaqa
=54ly
-----END PGP SIGNATURE-----
