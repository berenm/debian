-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: dwarf-fortress-mod-mayday
Binary: dwarf-fortress-mod-mayday
Architecture: all
Version: 0.34.11.0-3
Maintainer: Beren Minor <beren.minor+debian@gmail.com>
Homepage: http://www.bay12games.com/dwarves/
Standards-Version: 3.9.3
Vcs-Browser: https://github.com/berenm/dwarf-fortress/
Vcs-Git: git://github.com/berenm/dwarf-fortress.git
Build-Depends: debhelper (>= 9), python
Package-List: 
 dwarf-fortress-mod-mayday deb non-free/games extra
Checksums-Sha1: 
 b97316836fecd2e215a87564feb9cdad4010956a 685238 dwarf-fortress-mod-mayday_0.34.11.0.orig.tar.gz
 9080541684c4f6f9dbc402e370b2193e7ff55503 2282 dwarf-fortress-mod-mayday_0.34.11.0-3.debian.tar.gz
Checksums-Sha256: 
 23c2514ec44fd3e7af524a54e14dbd6c9a90a04bfc31a6f5a9de80c7483f176e 685238 dwarf-fortress-mod-mayday_0.34.11.0.orig.tar.gz
 7d30a212f923fd81a281a2b677be04092a109bc78f4af2890cd7c135ca9b5f74 2282 dwarf-fortress-mod-mayday_0.34.11.0-3.debian.tar.gz
Files: 
 c6638ee41d040a03c18a11817a034b48 685238 dwarf-fortress-mod-mayday_0.34.11.0.orig.tar.gz
 e63a435bbd75dd46faf76aa0a19950c3 2282 dwarf-fortress-mod-mayday_0.34.11.0-3.debian.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.12 (GNU/Linux)

iQIcBAEBAgAGBQJP44T6AAoJEIX7t2k16B/FC4oP/28QE2oE7m0Mv4PPp9Jmsd8n
yd21trTAXLrHn122Tx8MWuUebANTgT56RWOtD5xgtAKYxLZoDn6vWMa7TV0d/p25
sMGw0hvQZ26M+qvP9e+kZvnb9nNGlMC2dybEM+7pyorq4f1TiN6r45Wdu1k4qGcI
oMpIVAMOUoSKhVo3nHRyZQTD3yfWBM9r0VL7x9T6eE6fpYRKHBafsKEYc69w8GW6
DqFPwoWnaGxrEmkcdEJa0fYDc9ACWa7bBZAitUgBJi8E0SWzt154Xpd0EL65hzeH
53zsLHc4FaZ+UIXW27Nn8GjDsh4FxgxshbiZDRiFFmBWQBHmC+wJWGZnAjfQVEhO
yYUYs7Vn54L99a7hVoPTNSfUGugQUMNaxr+PpUyTC/XqBTAj37/qPrHdrdMvD0iw
Ra+bhi9sTcG7yUhMLCyDAyDoy/LiEnSw2nbn2Mqcu6AWdBnK5AUxMC85MrLTBA//
yOq5M4ldF6N5ybDvWL7FDphj3EEr+XYIt2JzeVSBkJQPZECRks81/hqSYBbNXzaM
7EpNRZcO/NYxGide/L7KDhj4BjW8YUT0Agb22qHHMe+wyE86ZbRgjzhF2vF+EoHw
aNCQwPOXjt3DWGMHUh89KHBp3nZ2Sgw6U+GKcuXfzkH4umG8ypZN4+hR7nwafcB5
VsuZJvmp96WhiOYHEdsq
=ILZp
-----END PGP SIGNATURE-----
