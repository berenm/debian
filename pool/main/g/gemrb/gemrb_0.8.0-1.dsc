-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: gemrb
Binary: gemrb, libgemrb, gemrb-doc, gemrb-data, gemrb-baldurs-gate-data, gemrb-baldurs-gate, gemrb-baldurs-gate-2-data, gemrb-baldurs-gate-2, gemrb-icewind-dale-data, gemrb-icewind-dale, gemrb-icewind-dale-2-data, gemrb-icewind-dale-2, gemrb-planescape-torment-data, gemrb-planescape-torment
Architecture: any all
Version: 0.8.0-1
Maintainer: Beren Minor <beren.minor+debian@gmail.com>
Homepage: http://www.gemrb.org/
Standards-Version: 3.9.3
Vcs-Browser: https://github.com/berenm/gemrb
Vcs-Git: git://github.com/berenm/gemrb.git
Build-Depends: debhelper (>= 9), cmake, python-dev, libsdl1.2-dev, libsdl-ttf2.0-dev, libsdl-mixer1.2-dev, libz-dev, libopenal-dev, libpng-dev, libogg-dev, libvorbis-dev
Package-List: 
 gemrb deb games extra
 gemrb-baldurs-gate deb games extra
 gemrb-baldurs-gate-2 deb games extra
 gemrb-baldurs-gate-2-data deb games extra
 gemrb-baldurs-gate-data deb games extra
 gemrb-data deb games extra
 gemrb-doc deb doc extra
 gemrb-icewind-dale deb games extra
 gemrb-icewind-dale-2 deb games extra
 gemrb-icewind-dale-2-data deb games extra
 gemrb-icewind-dale-data deb games extra
 gemrb-planescape-torment deb games extra
 gemrb-planescape-torment-data deb games extra
 libgemrb deb games extra
Checksums-Sha1: 
 d12136f9a5211a2223e41a815526829f22759ac1 12231959 gemrb_0.8.0.orig.tar.gz
 86c8860ba9c23e9857bed6a7767f097a183e91bf 1482049 gemrb_0.8.0-1.debian.tar.gz
Checksums-Sha256: 
 c0269877826702f50dd2c99c3059aecc2ac3e868dfb39357388da6eb66fe1bde 12231959 gemrb_0.8.0.orig.tar.gz
 e37ff59765924740d157cf208216ae59b14eb3d2b8d728792399e8fcf53e11eb 1482049 gemrb_0.8.0-1.debian.tar.gz
Files: 
 3dc9dbd6693b1aad67200608f3206ad5 12231959 gemrb_0.8.0.orig.tar.gz
 9fc81968ca8925857ab5a172e5a20fc7 1482049 gemrb_0.8.0-1.debian.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.12 (GNU/Linux)

iQIcBAEBAgAGBQJRoy1JAAoJEIX7t2k16B/FeEoQAJKyf8AsWyYTsxG3XyA8wOoY
7DhvsYsSYb8YtiKAP5cJD1Jm2JbycF+zVULVsJnpukszJhQ1PzMoSumbkOMlfjuj
btGAOJ8qdAkvsCIEnM9GaLu0M0Y4C7kbHwATx6WWKo+AgKuu8ENv4ABlTTHXn7aB
bH0LpMGksJEv2DGcxT7vxpnhwFgYBR/0mn7OiGEaV7Gs7D47mJBLtSzAy2Jwv47W
/E50zNGUZaFMZ3WHejAe1/J4reRnabF6uX0omFk+YkBJ+610qvPk2fiU/dUsEbpe
rhcOMAfodpNbTbEAHdhpB6IKZwlbJJYF5/nE/3UjmD1iPYAcrj9exDNIV0zNn52G
vW6aYyt/RooBpn1ed5rcYN7VtdNr8HzdTfMdwgR4pHpEodrU3eIVk0705gnwEdSd
z1DgboidKFXtJGGk7ZEvgpxVqnJbokgd/RZcjNpjQxj7BsZRPCCBc5/g8Yj1C+dZ
PCbnlZ/5vPl4VvByatLo3Fk0nkXo+XEny3Td2r0RfEyrKeRgHjHu2SPxmEEzprdD
FHNprs4tHoR/X7b4RxukND3cq6ejJh6D8kxN4jCFA3r5VPGb8vvnIfuQ0qwj5snU
GGE8qU+UYgTu6xiAya07vEyOWvfzKKq1QlofMm7DNXiO/0Ip5OksmoLSOoOdJFCT
S6qtu3vvppIu6KOlHsjO
=Tlje
-----END PGP SIGNATURE-----
