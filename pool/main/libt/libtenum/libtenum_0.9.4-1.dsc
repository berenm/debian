Format: 3.0 (quilt)
Source: libtenum
Binary: libtenum-dev
Architecture: any
Version: 0.9.4-1
Maintainer: Beren Minor <beren.minor+debian@gmail.com>
Homepage: http://berenm.github.com/libtenum
Standards-Version: 3.9.3
Vcs-Browser: https://github.com/berenm/libtenum
Vcs-Git: git://github.com/berenm/libtenum.git
Build-Depends: debhelper (>= 8.0.0), cmake, python, libboost-all-dev
Package-List: 
 libtenum-dev deb libdevel extra
Checksums-Sha1: 
 83ac76e0a64e2c9f8cbd401868bba8e557a1372e 32754 libtenum_0.9.4.orig.tar.gz
 9bc73c8009e6741ee94b3ed9d460b586f9f84c09 2670 libtenum_0.9.4-1.debian.tar.gz
Checksums-Sha256: 
 5aac4adb31511fc4e519c2b281e9d97e747d3e41c069f2917e2c1fd30d173e2b 32754 libtenum_0.9.4.orig.tar.gz
 4687069a8d45be7efe1e4e7f174c82cf48050d1b120f440cf350ee673d0a0e71 2670 libtenum_0.9.4-1.debian.tar.gz
Files: 
 05380e180d1ebcd3d68fe698825c8e3e 32754 libtenum_0.9.4.orig.tar.gz
 d8eaee883e191377e020438ea398661e 2670 libtenum_0.9.4-1.debian.tar.gz
